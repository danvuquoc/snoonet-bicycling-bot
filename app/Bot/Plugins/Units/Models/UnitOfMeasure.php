<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/20/17
 * Time: 6:50 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Units\Models;

use Illuminate\Support\Collection;
use PhpUnitsOfMeasure\AbstractPhysicalQuantity;
use PhpUnitsOfMeasure\PhysicalQuantity\Area;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Pressure;
use PhpUnitsOfMeasure\PhysicalQuantity\Velocity;
use PhpUnitsOfMeasure\PhysicalQuantity\Temperature;
use PhpUnitsOfMeasure\PhysicalQuantity\Volume;
use Exception;
use PhpUnitsOfMeasure\PhysicalQuantityInterface;

class UnitOfMeasure
{
    /**
     * An array of AbstractPhysicalQuantity classes we'll cycle through in order to auto detect tokens.
     */
    const PHYSICAL_QUANTITIES = [
        Length::class,
        Velocity::class,
        Mass::class,
        Temperature::class,
        Area::class,
        Pressure::class,
        Volume::class,
    ];

    /**
     * A basic mapping of things we should be attempting to map.
     */
    const UNIT_CONVERSION = [
        'm' => 'ft',
        'cm' => 'in',
        'km' => 'mi',
        'kg' => 'lbs',
        'C' => 'F',
        'km/h' => 'mph',
        'bar' => 'psi',
        'm^2' => 'ft^2',
        'l' => 'qt',
    ];

    /**
     * @var string The raw text that was used to create the unit of measure.
     */
    protected $raw;

    /**
     * @var float | int The amount of the unit.
     */
    protected $quantity;

    /**
     * @var string The unit type.
     */
    protected $unit;

    /**
     * @var AbstractPhysicalQuantity The object created as a result of constructing the unit of measure.
     */
    protected $physicalQuantity;

    /**
     * @var null | string The target unit based on the source unit.
     */
    protected $targetUnit;


    /**
     * UnitOfMeasure constructor.
     * @param $raw
     * @param $quantity
     * @param $unit
     */
    public function __construct($raw, $quantity, $unit)
    {
        $this->raw = $raw;
        $this->quantity = $quantity;
        $this->unit = $unit;

        if ($this->physicalQuantity = self::createPhysicalQuantity($this->quantity, $this->unit)) {
            if ($sourceUnitOfMeasure = self::searchUnitsOfMeasure($this->unit)) {
                $searchUnits = [];
                $searchUnits[] = $sourceUnitOfMeasure->getName();
                $searchUnits = array_merge($searchUnits, $sourceUnitOfMeasure->getAliases());
                if ($unitTarget = self::searchConversion($searchUnits)) {
                    $this->targetUnit = $unitTarget;
                }
            }
        }
    }

    /**
     * Takes a line of text and returns units switched over if applicable.
     * @param $text
     * @return mixed
     */
    public static function switchUnits($text)
    {
        $units = self::find($text)->all();
        foreach ($units as $unit) {
            $text = preg_replace('@'.$unit->rawRegex().'@msi', $unit->convert(2), $text);
        }

        return $text;
    }

    /**
     * Attempt to create a Physical Quantity cycling through available classes.
     * @param $quantity
     * @param $unit
     * @return null | PhysicalQuantityInterface
     */
    public static function createPhysicalQuantity($quantity, $unit)
    {
        foreach (self::PHYSICAL_QUANTITIES as $physicalQuantityClass) {
            try {
                if ($physicalQuantityClass::getUnit($unit)) {
                    return new $physicalQuantityClass($quantity, $unit);
                }
            } catch (Exception $e) {
                continue;
            }
        }

        return null;
    }

    /**
     * Search our physical quantities class list to try and create a valid object.
     * @param $unit
     * @return null | PhysicalQuantityInterface
     */
    public static function searchUnitsOfMeasure($unit)
    {
        foreach (self::PHYSICAL_QUANTITIES as $physicalQuantityClass) {
            try {
                if ($physicalQuantityClass::getUnit($unit)) {
                    foreach ($physicalQuantityClass::getUnitDefinitions() as $unitOfMeasureDefinition) {
                        if ($unit == $unitOfMeasureDefinition->getName() || $unitOfMeasureDefinition->isAliasOf($unit)) {
                            return $unitOfMeasureDefinition;
                        }
                    }
                }
            } catch (Exception $e) {
                continue;
            }
        }

        return null;
    }

    /**
     * Search our mapping of units of measure to see if we can convert to an appropriate unit.
     * @param $units
     * @return int|mixed|null|string
     */
    public static function searchConversion($units)
    {
        $units = array_map(function ($value) {
            return strtoupper($value);
        }, $units);

        foreach (self::UNIT_CONVERSION as $metric => $imperial) {
            if (in_array(strtoupper($metric), $units)) {
                // We have metric so return imperial.
                return $imperial;
            } elseif (in_array(strtoupper($imperial), $units)) {
                // We have imperial so return metric.
                return $metric;
            }
        }

        return null;
    }

    /**
     * Convert the unit of measure to the target unit of measure.
     * @param int $precision
     * @return null|string
     */
    public function convert($precision = 2)
    {
        if ($this->targetUnit) {
            return $this->convertQuantity($precision).' '.$this->targetUnit;
        } else {
            return null;
        }
    }

    /**
     * Get the target unit to be converted to.
     * @return null|string
     */
    public function convertUnit()
    {
        return $this->targetUnit;
    }

    /**
     * Convert just the quantity.
     * @param int $precision
     * @return float|null
     */
    public function convertQuantity($precision = 2)
    {
        if ($this->targetUnit) {
            return round($this->physicalQuantity->toUnit($this->targetUnit), $precision);
        } else {
            return null;
        }
    }

    /**
     * Create a Collection of units of measure.
     * @param $text
     * @return Collection
     */
    public static function find($text)
    {
        $units = new Collection();
        if ($tokens = preg_split('@[\s,]+@msi', $text)) {
            while ($tokens) {
                // Take an item off of the array.
                $token = array_shift($tokens);

                // Strip off the period at the end of a token if there is one.
                $token = preg_replace('@\.$@msi', '', $token);

                if (preg_match('@^(\d*\.?\d+)([a-z\/\^]+[23]?)@si', $token, $matches)) {
                    // eg.) 30mph, 30.1mph, 3m, .5m
                    // We'll need to split the token into its two parts.
                    $unit = new UnitOfMeasure($matches[0], $matches[1], $matches[2]);
                    if ($unit->isValid()) {
                        $units->push($unit);
                    }
                } elseif (preg_match('@^(\d*\.?\d+)$@msi', $token)) {
                    // eg.) 30, 30.1, 0.5, .5
                    // We'll need to grab the next token if possible.
                    if ($nextToken = array_shift($tokens)) {

                        // Strip off the period at the end of a token if there is one.
                        $nextToken = preg_replace('@\.$@msi', '', $nextToken);

                        if (preg_match('@^([a-z\/\^]+[23]?)$@msi', $nextToken)) {
                            $unit = new UnitOfMeasure($token.' '.$nextToken, $token, $nextToken);
                            if ($unit->isValid()) {
                                $units->push($unit);
                            }
                        }
                    }
                }
            }
        }
        return $units;
    }

    /**
     * Is the unit valid?
     * @return bool
     */
    public function isValid()
    {
        if ($this->physicalQuantity && $this->targetUnit) {
            return true;
        }
        return false;
    }

    /**
     * Create a regex for the original unit of measure detected.
     * @return mixed|string
     */
    public function rawRegex()
    {
        $raw = $this->raw;
        $regex = preg_quote($raw);
        $regex = preg_replace('@\s+@msi', '\s', $regex);
        return $regex;
    }


    /**
     * Is the unit metric?
     * @return bool
     */
    public function isMetric()
    {
        foreach (array_keys(self::UNIT_CONVERSION) as $unit) {
            if ($unit == $this->unit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Is the unit imperial?
     * @return bool
     */
    public function isImperial()
    {
        foreach (self::UNIT_CONVERSION as $unit) {
            if ($unit == $this->unit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the physical property.
     * @return null|AbstractPhysicalQuantity
     */
    public function getPhysicalProperty()
    {
        return $this->physicalQuantity;
    }
}
