<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/19/17
 * Time: 8:59 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Units;

use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Illuminate\Support\Collection;
use Phergie\Irc\Event\UserEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Danvuquoc\BicyclingBot\Bot\Plugins\Units\Models\UnitOfMeasure;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string
     */
    protected $command = 'units';

    /**
     * @var Collection[] An array of collections.
     */
    protected $histories = [];

    /**
     * @var int The maximum amount of buffer to keep for each source.
     */
    protected $buffer = 50;

    /**
     * Use the basica subcommand router as well as listen to all privmsgs.
     * @return array The subscriber array.
     */
    public function getSubscribedEvents()
    {
        $events = parent::getSubscribedEvents();
        $events['irc.received.privmsg'] = 'listen';
        $events['irc.sent.privmsg'] = 'listen';
        return $events;
    }

    /**
     * @return array Routing array
     */
    public function routing()
    {
        return [
            '' => 'last',
            '*' => 'last',
            'help' => 'help'
        ];
    }

    /**
     * Store a small buffer of each source to check for units.
     * @param UserEventInterface $event
     */
    public function listen(UserEventInterface $event)
    {
        // Finds if there are units to convert first, no sense in storing lines that don't need to be.
        $unitsToConvert = UnitOfMeasure::find((string) $event->getParams()['text']);
        if ($unitsToConvert->isNotEmpty()) {

            $source = $event->getSource();

            // Make sure we initialize a new collection if necessary.
            if (!isset($this->histories[$source])) {
                $this->histories[$source] = new Collection();
            }

            // Figure out if we need to remove an element from the collection.
            if ($this->histories[$source]->count() >= $this->buffer) {
                $this->histories[$source]->shift();
            }

            $nick = $event->getNick() ? $event->getNick() : $event->getConnection()->getNickname();

            // Push the element into the history.
            $this->histories[$source]->push([
                'nick' => $nick,
                'text' => '<'.$nick.'> '.$event->getParams()['text'],
                'ts' => time()
            ]);
        }
    }

    /**
     * Extract the last line either specified by a nick or spoken in general.
     * @param $source
     * @param string|null $nick
     * @return string|null
     */
    public function getLastChannelLine($source, $nick = null)
    {
        if (isset($this->histories[$source])) {
            if ($nick && $line = $this->histories[$source]->where('nick', $nick)->sortBy('ts')->last()) {
                return $line['text'];
            } elseif (!$nick && $line = $this->histories[$source]->sortBy('ts')->last()) {
                return $line['text'];
            }
        }

        return null;
    }

    /**
     *
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function last(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        if ($lastLine = $this->getLastChannelLine($event->getSource(), $commandArgument)) {
            $queue->ircPrivmsg($event->getSource(), UnitOfMeasure::switchUnits($lastLine));
        } else if ($commandArgument) {
            $queue->ircPrivmsg($event->getSource(), UnitOfMeasure::switchUnits($commandArgument));
        } else {
            $queue->ircPrivmsg($event->getSource(), 'Not enough chat data yet.');
        }
    }

    /**
     * Output help text.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function help(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        $messages = [
            'This command finds recent units of measure and flips metric and imperial quantities.',
            "Usage: !{$this->command}, !{$this->command} [nick], !{$this->command} [string]"
        ];

        foreach ($messages as $message) {
            $queue->ircPrivmsg($event->getSource(), $message);
        }
    }
}
