<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/15/17
 * Time: 10:18 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Photo;

use Danvuquoc\BicyclingBot\Bot\Plugins\Photo\Models\Photo;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Exception;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string Command
     */
    protected $command = 'photo';

    /**
     * @return array Routing
     */
    public function routing()
    {
        return [
            '' => 'getRequesterPhoto',
            '*' => 'getUserPhoto',
            'set' => 'setPhoto',
            'unset' => 'unsetPhoto',
        ];
    }

    /**
     * Get the photo of the user requesting.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function getRequesterPhoto(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $this->getUserPhoto($event, $queue, $event->getNick());
    }

    /**
     * Get the stored photo for the current user.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $nick
     */
    public function getUserPhoto(CommandEventInterface $event, EventQueueInterface $queue, $nick)
    {
        $photo = Photo::whereSource($event->getSource())
            ->whereNick($nick)
            ->first();

        if ($photo) {
            $queue->ircPrivmsg(
                $event->getSource(),
                (string) $photo
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s doesn't have a photo set.",
                    $nick
                )
            );
        }
    }

    /**
     * Unset a photo.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function unsetPhoto(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $photo = Photo::whereSource($event->getSource())
            ->whereNick($event->getNick())
            ->first();
        if ($photo) {
            $photo->delete();
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your photo has been removed.",
                    $event->getNick()
                )
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, you don't have a photo set.",
                    $event->getNick()
                )
            );
        }
    }

    /**
     * Set a photo.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function setPhoto(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        try {
            // Create and save the photo.
            $photo = new Photo();
            $photo->source = $event->getSource();
            $photo->nick = $event->getNick();
            $photo->content = $commandArgument;
            $photo->save();
            // Output confirmation message
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your photo has been added.",
                    $event->getNick()
                )
            );
        } catch (Exception $e) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry %s, an error occurred setting your photo: ".$e->getMessage(),
                    $event->getNick(),
                    $commandArgument
                )
            );
        }
    }
}
