<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/16/17
 * Time: 8:17 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Photo\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function __toString()
    {
        return $this->content;
    }
}
