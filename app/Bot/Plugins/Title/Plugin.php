<?php

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Title;

use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Phergie\Irc\Event\UserEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use League\Uri\Schemes\Http as HttpUri;
use Exception;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string The plugin's command
     */
    protected $command = 'title';

    /**
     * @var array An array of HttpUri objects keyed by source.
     */
    protected $lastUri = [];

    /**
     * @var array An array of strings representing title content keyed by source.
     */
    protected $lastTitle = [];

    /**
     * Add a listen handler to the default subscribed events router.
     * @return array
     */
    public function getSubscribedEvents()
    {
        $events = parent::getSubscribedEvents();
        $events['irc.received.privmsg'] = 'listen';
        return $events;
    }

    /**
     * Subcommand routing
     * @return array
     */
    public function routing()
    {
        return [
            '' => 'titleLast',
            '*' => 'titleUrl',
            'help' => 'help',
        ];
    }

    /**
     * Listen to source and store the last uri/title.
     * @param UserEventInterface $event
     */
    public function listen(UserEventInterface $event)
    {
        // Split apart the full parameter text.
        $params = preg_split('@\s+@msi', $event->getParams()['text']);
        // Find all the uris in the params
        if ($uri = self::findUris($params)->first()) {
            // If we can extract a title, assign it to the last uri and title.
            if ($title = self::extractTitle($uri)) {
                $this->lastUri[$event->getSource()] = $uri;
                $this->lastTitle[$event->getSource()] = $title;
            }
        }
    }

    /**
     * Handle a title command with a url specified.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $commandArgument
     */
    public function titleUrl(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        if ($uri = self::findUris($commandArgument)->first()) {
            $queue->ircPrivmsg($event->getSource(), self::extractTitle($uri));
        } else {
            $queue->ircPrivmsg($event->getSource(), "Unable to retrieve url from: ".$uri);
        }
    }

    /**
     * Handle a title command for the last url.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function titleLast(CommandEventInterface $event, EventQueueInterface $queue)
    {
        if ($this->getLastUri($event->getSource()) && $this->getLastTitle($event->getSource())) {
            $queue->ircPrivmsg($event->getSource(), $this->getLastTitle($event->getSource()));
        } else {
            $queue->ircPrivmsg($event->getSource(), "Sorry, I haven't seen any urls recently.");
        }
    }

    /**
     * Get the last uri.
     * @param string $source
     * @return string
     */
    public function getLastUri($source)
    {
        if (isset($this->lastUri[$source])) {
            return (string) $this->lastUri[$source];
        }
        return null;
    }

    /**
     * Get the last title.
     * @param string $source
     * @return string
     */
    public function getLastTitle($source)
    {
        if (isset($this->lastTitle[$source])) {
            return (string) $this->lastTitle[$source];
        }
        return null;
    }

    /**
     * Find all the uris in the event.
     * @param string|array $parameters
     * @return Collection
     */
    public static function findUris($parameters)
    {
        $uris = new Collection();

        // Figure out if we need to split on whitespaces.
        if (is_string($parameters)) {
            $parameters = preg_split('@\s+@msi', $parameters);
        }

        if ($parameters && is_array($parameters)) {
            foreach ($parameters as $param) {
                // Attempt to get a url out of each parameter.
                try {
                    $uri = HttpUri::createFromString($param);
                    if ($uri->getScheme() && $uri->getHost()) {
                        $uris->push($uri);
                    }
                } catch (Exception $e) {
                    continue;
                }
            }
        }

        return $uris;
    }

    /**
     * Help statement
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function help(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        // TODO Help
    }

    /**
     * Extract the title.
     * @param HttpUri $uri
     * @return null|string
     */
    public static function extractTitle(HttpUri $uri)
    {
        try {
            $client = new Client();
            $response = $client->get((string) $uri, [
                'headers' => [
                    'User-Agent' => 'Snoonet-Bicycling-Bot'
                ]
            ]);
            $crawler = new Crawler((string) $response->getBody());
            return trim($crawler->filter('title')->first()->text());
        } catch (Exception $e) {
            print($e->getMessage());
            return null;
        }
    }
}
