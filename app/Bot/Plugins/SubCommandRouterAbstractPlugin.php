<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/8/17
 * Time: 7:08 AM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins;

use Phergie\Irc\Bot\React\AbstractPlugin;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Log;

abstract class SubCommandRouterAbstractPlugin extends AbstractPlugin
{
    /**
     * @var string Must be defined by the implementing class to specify the command.
     */
    protected $command = '';

    /**
     * The events to subscribe to.
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'command.'.$this->command => 'route',
        ];
    }

    /**
     * Returns an associative array of subcommands to methods.
     * @return array
     */
    abstract public function routing();

    /**
     * Route events
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function route(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $params = $event->getCustomParams();
        $subCommand = array_first($params);
        Log::debug('Trying subcommand', [$subCommand]);

        if ($this->routing()) {
            foreach ($this->routing() as $potentialSubCommand => $method) {
                if ($potentialSubCommand != $subCommand) {
                    continue;
                }
                $arguments = $this->commandArguments($event, $subCommand);
                Log::debug('Subcommand routing', [
                    'method' => $method,
                    'arguments' => $arguments
                ]);
                $this->{$method}($event, $queue, $arguments);
                return;
            }

            if ($subCommand && isset($this->routing()['*'])) {
                $arguments = $this->commandArguments($event, '');
                Log::debug('Subcommand routing', [
                    'method' => $this->routing()['*'],
                    'arguments' => $arguments
                ]);
                $this->{$this->routing()['*']}($event, $queue, $arguments);
            } else if (isset($this->routing()[''])) {
                Log::debug('Subcommand routing', [
                    'method' => $this->routing()[''],
                    'arguments' => ''
                ]);
                $this->{$this->routing()['']}($event, $queue);
            }
        }
    }

    /**
     * Return everything after the initial command and subcommand.
     * @param CommandEventInterface $event
     * @param $subCommand
     * @return string
     */
    public function commandArguments(CommandEventInterface $event, $subCommand)
    {
        $params = $event->getParams();
        Log::debug('Subcommand', [$subCommand]);
        Log::debug('Subcommand Arguments', $event->getParams());
        $fullCommand = $params['text'];

        $regExp = '@';
        $regExp .= preg_quote(config('bot.command_prefix').$this->command);
        $regExp .= '\s+';
        $regExp .= $subCommand ? $subCommand : '';
        $regExp .= '\s*?';
        $regExp .= '(.*?)$';
        $regExp .= '@si';
        Log::debug('Subcommand routing regex', [$regExp]);

        if (preg_match($regExp, $fullCommand, $match)) {
            return trim($match[1]);
        }

        return '';
    }
}
