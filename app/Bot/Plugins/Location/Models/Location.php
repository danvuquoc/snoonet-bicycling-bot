<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/15/17
 * Time: 10:27 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location
 *
 * @property int $id
 * @property string $source
 * @property string $nick
 * @property string $unparsed
 * @property string $city
 * @property string $region
 * @property string $country
 * @property string $postal_code
 * @property float $latitude
 * @property float $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location wherePostalCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereUnparsed($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $timezone
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereTimezone($value)
 * @property string $measurement_system
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location whereMeasurementSystem($value)
 */
class Location extends Model
{
    /**
     * An array of countries using the imperial system.
     */
    const IMPERIAL_COUNTRIES = ['United States', 'Liberia', 'Myanmar'];

    /**
     * Public safe representation of the address.
     * @return string
     */
    public function __toString()
    {
        // Create a slice of public safe data.
        $safeAddressParts = array_intersect_key($this->toArray(), array_flip(['city', 'region', 'country']));
        // Remove any blank values.
        if ($safeAddressParts) {
            foreach ($safeAddressParts as $key => $value) {
                if (!$value) {
                    unset($safeAddressParts[$key]);
                }
            }
        }
        if ($safeAddressParts) {
            return implode(', ', $safeAddressParts);
        } else {
            return '';
        }
    }

    /**
     * Set the measurement system automatically by comparing the country to a list of imperial measurement countries.
     * @param string $value Country
     */
    public function setCountryAttribute($value)
    {
        $this->attributes['country'] = $value;
        $this->attributes['measurement_system'] = in_array($value, self::IMPERIAL_COUNTRIES) ? 'imperial' : 'metric';
    }
}
