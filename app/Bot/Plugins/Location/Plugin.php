<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/15/17
 * Time: 10:18 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Location;

use Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Exception;
use Geocoder\Exception\ChainNoResult;
use Geocoder\Exception\InvalidCredentials;
use Geocoder;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string Command
     */
    protected $command = 'location';

    /**
     * @return array Routing
     */
    public function routing()
    {
        return [
            '' => 'getRequesterLocation',
            '*' => 'getUserLocation',
            'set' => 'setLocation',
            'unset' => 'unsetLocation',
        ];
    }

    public function getRequesterLocation(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $this->getUserLocation($event, $queue, $event->getNick());
    }

    /**
     * Get the stored location for a current user.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $nick
     */
    public function getUserLocation(CommandEventInterface $event, EventQueueInterface $queue, $nick)
    {
        $location = Location::whereSource($event->getSource())
            ->whereNick($nick)
            ->first();

        if ($location) {
            $queue->ircPrivmsg(
                $event->getSource(),
                (string) $location
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s doesn't have a location set.",
                    $nick
                )
            );
        }
    }

    /**
     * Unset a location.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function unsetLocation(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $location = Location::whereSource($event->getSource())
            ->whereNick($event->getNick())
            ->first();
        if ($location) {
            $location->delete();
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your location has been removed.",
                    $event->getNick()
                )
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, you don't have a location set.",
                    $event->getNick()
                )
            );
        }
    }

    /**
     * Set a location.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function setLocation(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        try {
            // Geocode the input.
            $geocoded = Geocoder::geocode($commandArgument)->get()->first();
            // Create and save the location.
            $location = new Location();
            $location->city = $geocoded->getLocality();
            $location->region = $geocoded->getAdminLevels()->first()->getName();
            $location->country = $geocoded->getCountry()->getName();
            $location->postal_code = $geocoded->getPostalCode();
            $location->timezone = $geocoded->getTimezone();
            $location->latitude = $geocoded->getLatitude();
            $location->longitude = $geocoded->getLongitude();
            $location->nick = $event->getNick();
            $location->source = $event->getSource();
            $location->unparsed = $commandArgument;
            $location->save();
            // Output confirmation message
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your location has been added.",
                    $event->getNick()
                )
            );
        } catch (InvalidCredentials $e) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry %s, I can't lookup %s because I haven't been properly configured.",
                    $event->getNick(),
                    $commandArgument
                )
            );
        } catch (ChainNoResult $e) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry %s, I couldn't find %s.",
                    $event->getNick(),
                    $commandArgument
                )
            );
        } catch (Exception $e) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry %s, an error occurred setting your location: ".$e->getMessage(),
                    $event->getNick(),
                    $commandArgument
                )
            );
        }
    }
}
