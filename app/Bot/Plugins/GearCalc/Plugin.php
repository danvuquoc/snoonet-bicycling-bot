<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 1/8/17
 * Time: 10:28 AM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\GearCalc;

use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string Command
     */
    protected $command = 'gearcalc';

    /**
     * @return array Routing
     */
    public function routing()
    {
        return [
            '*' => 'calculator',
            '' => 'help',
            'help' => 'help',
        ];
    }

    public function calculator(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        $measurementSystem = Location::whereSource($event->getSource())
            ->whereNick($event->getNick())
            ->value('measurement_system');

        $calculator = new Calculator($commandArgument, $measurementSystem == 'metric' ? true : false);
        $queue->ircPrivmsg($event->getSource(), $calculator->solve());
    }

    public function help(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $calculator = new Calculator('');
        $queue->ircPrivmsg($event->getSource(), $calculator->solve());
    }
}
