<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 1/8/17
 * Time: 10:34 AM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\GearCalc;

use Illuminate\Support\Collection;
use PhpUnitsOfMeasure\PhysicalQuantity\Velocity;
use Exception;

class Calculator
{
    /**
     * @var Collection The tokens input into the calculator.
     */
    protected $tokens;

    /**
     * @var array Setup array with all the variables needed for the calculator.
     */
    protected $variables = [
        [
            'name' => 'Cadence',
            'find_method' => 'findCadence',
            'solve_method' => 'solveCadence',
            'examples' => ['90rpm'],
        ],
        [
            'name' => 'Speed',
            'find_method' => 'findSpeed',
            'solve_method' => 'solveSpeed',
            'examples' => ['20mph', '32km/h'],
        ],
        [
            'name' => 'Chainring Size',
            'find_method' => 'findFrontTeeth',
            'solve_method' => 'solveFrontTeeth',
            'examples' => ['53x11', '50x?'],
        ],
        [
            'name' => 'Cog Size',
            'find_method' => 'findRearTeeth',
            'solve_method' => 'solveRearTeeth',
            'examples' => ['53x11', '?x11'],
        ]
    ];

    /**
     * @var bool Should we output in metric or imperial
     */
    protected $metric;

    /**
     * Calculator constructor.
     * @param $initializationString
     * @param bool $metric
     */
    public function __construct($initializationString, $metric = true)
    {
        // Tokenize
        $this->tokenize($initializationString);

        // Default
        $this->metric = $metric ? true : false;

        if ($this->findMetric()) $this->metric = true;
        if ($this->findImperial()) $this->metric = false;
    }

    /**
     * The text solution to the calculator's current parameters.
     * @return string
     */
    public function solve()
    {
        $missingVariables = $this->missingVariables();

        if ($missingVariables->count() == 1) {
            $missingVariable = $this->missingVariables()->first();
            return $this->{$missingVariable['solve_method']}();
        } else if ($missingVariables->count() > 1) {
            // Find the missing variables we need as a help message.
            $returnText = 'More information is needed: ';
            $variableText = [];
            foreach ($missingVariables as $missingVariable) {
                $variableText[] = $missingVariable['name'] . ' (eg. '.implode(', ', $missingVariable['examples']).')';
            }
            $variableText = $variableText ? implode('; ', $variableText) : '';
            return $returnText . $variableText;
        } else {
            return "I think you've already solved your own question.";
        }
    }

    /**
     * Can the calculator be solved?
     * @return bool
     */
    public function isSolvable()
    {
        $missingVariables = $this->missingVariables();
        return $missingVariables->count() == 1 ? true : false;
    }

    /**
     * Finds the missing variables.
     * @return Collection
     */
    protected function missingVariables()
    {
        $missing = new Collection();
        foreach ($this->variables as $variable) {
            if (!$this->{$variable['find_method']}()) $missing->push($variable);
        }
        return $missing;
    }

    /**
     * Process an input string and populate the tokens collection.
     * @param $string
     */
    protected function tokenize($string)
    {
        $parts = preg_split('@\s+@msi', $string);
        $this->tokens = new Collection($parts);
    }

    /**
     * Finds the cadence.
     * @return int|null
     */
    protected function findCadence()
    {
        foreach ($this->tokens as $token) {
            if (preg_match('@^(\d+)rpm$@msi', $token, $matches)) {
                if ($matches[1] != '?') return (int) $matches[1];
            }

        }
        return null;
    }

    /**
     * Finds the speed.
     * @return null|Velocity
     */
    protected function findSpeed()
    {
        foreach ($this->tokens as $token) {
            if (preg_match('@^(\d*\.?\d+)([a-z\/\^]+[23]?)@msi', $token, $matches)) {
                $quantity = $matches[1];
                $unit = $matches[2];
                foreach (Velocity::getUnitDefinitions() as $unitDefinition) {
                    try {
                        if ($unit == $unitDefinition->getName() || $unitDefinition->isAliasOf($unit)) {
                            return new Velocity($quantity, $unit);
                        }
                    } catch (Exception $e) {
                        continue;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Find the number of front teeth specified.
     * @return null|int
     */
    protected function findFrontTeeth()
    {
        foreach ($this->tokens as $token) {
            if (preg_match('@^([\d\?]+)x[\d\?]+$@msi', $token, $matches)) {
                if ($matches[1] != '?') return (int) $matches[1];
            }

        }
        return null;
    }

    /**
     * Find the number of rear teeth specified.
     * @return null|int
     */
    protected function findRearTeeth()
    {
        foreach ($this->tokens as $token) {
            if (preg_match('@^[\d\?]+x([\d\?]+)$@msi', $token, $matches)) {
                if ($matches[1] != '?') return (int) $matches[1];
            }
        }
        return null;
    }

    /**
     * Find the wheel circumference.
     * @return int
     */
    protected function findWheelCircumference()
    {
        foreach ($this->tokens as $token) {
            if (preg_match('@^(\d{2})-(\d{3})$@msi', $token, $matches)) {
                return (int) round(($matches[2] + ($matches[1] * 2)) * pi());
            }
        }
        // Default to a 700c rim with 23mm tires.
        return (int) round((622 + (23 * 2)) * pi());
    }

    /**
     * Was imperial specified?
     * @return bool
     */
    protected function findImperial()
    {
        foreach ($this->tokens as $token) {
            if ($token == 'imperial') return true;
        }

        return false;
    }

    /**
     * Was metric specified?
     * @return bool
     */
    protected function findMetric()
    {
        foreach ($this->tokens as $token) {
            if ($token == 'metric') return true;
        }

        return false;
    }

    /**
     * Calculate the gear ratio of the system.
     * @return float|int
     */
    protected function solveGearRatio()
    {
        return $this->findFrontTeeth() / $this->findRearTeeth();
    }

    /**
     * @return float|int
     */
    protected function solveMetersDevelopment()
    {
        return $this->solveGearRatio() * ($this->findWheelCircumference() / 1000);
    }

    /**
     * Solve speed.
     * @return string
     */
    protected function solveSpeed()
    {
        // Speed/s = Gear Ratio * Wheel Circumference * Cadence/s
        // Speed/s = Meters Development * Cadence

        $md = $this->solveMetersDevelopment();
        $c = $this->findCadence() / 60;
        $x = $md * $c;
        $speed = new Velocity($x, 'm/s');
        if ($this->metric) {
            return round($speed->toUnit('km/h'), 1) . ' km/h';
        } else {
            return round($speed->toUnit('mph'), 1) . ' mph';
        }
    }

    /**
     * Solve cadence.
     * @return string
     */
    protected function solveCadence()
    {
        // Speed/s = Gear Ratio * Wheel Circumference * Cadence/s
        // Speed/s = Meters Development * Cadence/s
        // Speed/s / Meters Development = Cadence/s
        $s = $this->findSpeed()->toUnit('m/s');
        $md = $this->solveMetersDevelopment();
        $x = ($s / $md) * 60;
        return round($x, 0) . ' rpm';
    }

    /**
     * Solve front teeth
     * @return string
     */
    protected function solveFrontTeeth()
    {
        // Speed/s = Gear Ratio * Wheel Circumference * Cadence/s
        // Speed/s = (Front Teeth / Rear Teeth) * Wheel Circumference * Cadence/s
        // ((Speed/s / Cadence/s) / Wheel Circumference) = (Front Teeth / Rear Teeth)
        // ((Speed/s / Cadence/s) / Wheel Circumference) * Rear Teeth = Front Teeth
        $rt = $this->findRearTeeth();
        $s = $this->findSpeed()->toUnit('m/s');
        $c = $this->findCadence() / 60;
        $wc = $this->findWheelCircumference() / 1000;
        $x = $rt * (($s / $c) / $wc);
        return round($x, 0) . 'T chainring';
    }

    /**
     * Solve rear teeth.
     * @return string
     */
    protected function solveRearTeeth()
    {
        $ft = $this->findFrontTeeth();
        $s = $this->findSpeed()->toUnit('m/s');
        $c = $this->findCadence() / 60;
        $wc = $this->findWheelCircumference() / 1000;
        $x = ($wc * $ft * $c) / $s;
        return round($x, 0) . 'T cog';
    }
}
