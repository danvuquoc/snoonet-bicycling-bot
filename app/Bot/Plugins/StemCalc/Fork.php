<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 4/8/17
 * Time: 3:16 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc;


class Fork
{
    /**
     * @var float Mounted headtube angle.
     */
    protected $mountedAngle;

    /**
     * Creates a fork for fluent syntax.
     * @return Fork
     */
    public static function create()
    {
        return new self;
    }

    /**
     * Set the mounted headtube angle.
     * @param float $mountedAngle
     * @return $this
     */
    public function setMountedAngle(float $mountedAngle)
    {
        $this->mountedAngle = $mountedAngle;
        return $this;
    }

    /**
     * Get the mounted headtube angle.
     * @return float
     */
    public function getMountedAngle()
    {
        return $this->mountedAngle;
    }
}