<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 4/8/17
 * Time: 3:14 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc;

use Exception;

class Stem
{
    /**
     * @var int
     */
    protected $length;

    /**
     * @var float
     */
    protected $angle;

    /**
     * @var int
     */
    protected $height = 40;

    /**
     * @var int
     */
    protected $spacerStack;

    /**
     * @var Fork
     */
    protected $fork;

    /**
     * Creates a stem for fluent syntax.
     * @return Stem
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @param int $length
     * @return $this
     */
    public function setLength(int $length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @param float $angle
     * @return $this
     */
    public function setAngle(float $angle)
    {
        $this->angle = $angle;
        return $this;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @param int $spacerStack
     * @return $this
     */
    public function setSpacerStack(int $spacerStack)
    {
        $this->spacerStack = $spacerStack;
        return $this;
    }

    /**
     * @param Fork $fork
     * @return $this
     */
    public function setFork(Fork $fork)
    {
        $this->fork = $fork;
        return $this;
    }

    /**
     * Calculate the reach.
     * @return float
     */
    public function reach()
    {
        $reach = $this->length * cos($this->effectiveAngleRadians());
        if ($this->spacerStack || $this->height) {
            $stackOfSpacersAndStem = Stem::create()->setLength($this->spacerStack + ($this->height / 2))
                ->setAngle(90 - $this->fork->getMountedAngle())
                ->setHeight(0)
                ->setSpacerStack(0)
                ->setFork(Fork::create()->setMountedAngle(90))
                ->stack();
            return $reach - $stackOfSpacersAndStem;
        } else {
            return $reach;
        }
    }

    /**
     * Calculate the stack.
     * @return float
     */
    public function stack()
    {
        $stack = $this->length * sin($this->effectiveAngleRadians());
        if ($this->spacerStack || $this->height) {
            $reachOfSpacersAndStem = Stem::create()->setLength($this->spacerStack + ($this->height / 2))
                ->setAngle(90 - $this->fork->getMountedAngle())
                ->setHeight(0)
                ->setSpacerStack(0)
                ->setFork(Fork::create()->setMountedAngle(90))
                ->reach();
            return $stack + $reachOfSpacersAndStem;
        } else {
            return $stack;
        }
    }

    /**
     * Give the effective angle in degrees from horizontal.
     * @return int
     * @throws Exception When the fork is incorrectly configured.
     */
    protected function effectiveAngleDegrees()
    {
        if (!$this->fork) throw new Exception("Fork must be set to calculate effective angle.");
        if (!$this->fork->getMountedAngle()) throw new Exception("Fork's mounted angle must be set to calculate effective angle.");
        $effectiveAngle = 90 - $this->fork->getMountedAngle() + $this->angle;
        return $effectiveAngle;
    }

    /**
     * Give the effective angle in radians from horizontal.
     * @return float
     */
    public function effectiveAngleRadians()
    {
        return deg2rad($this->effectiveAngleDegrees());
    }

}