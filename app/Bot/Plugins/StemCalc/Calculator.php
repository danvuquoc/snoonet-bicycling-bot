<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 1/8/17
 * Time: 10:34 AM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc;

use Illuminate\Support\Collection;
use Exception;

class Calculator
{
    /**
     * @var Collection The tokens input into the calculator.
     */
    protected $tokens;

    /**
     * @var array Setup array with all the variables needed for the calculator.
     */
    protected $variables = [
        [
            'name' => 'First Stem',
            'find_method' => 'findFirstStem',
            'examples' => ['100/-6/25', '100/-6/25/40', 'Format: {stem length}/{stem angle}/{stem spacer stack}[/{stem height}'],
        ],
        [
            'name' => 'Second Stem',
            'find_method' => 'findSecondStem',
            'examples' => ['100/-6/25', '100/-6/25/40', 'Format: {stem length}/{stem angle}/{stem spacer stack}[/{stem height}'],
        ],
        [
            'name' => 'Fork Angle',
            'find_method' => 'findFork',
            'examples' => ['72.5', '73'],
        ],
    ];

    /**
     * Calculator constructor.
     * @param $initializationString
     */
    public function __construct($initializationString)
    {
        // Tokenize
        $this->tokenize($initializationString);

    }

    /**
     * The text solution to the calculator's current parameters.
     * @return string
     */
    public function solve()
    {
        // Figure out if we're missing information to solve.
        $missingVariables = $this->missingVariables();

        if ($missingVariables->count()) {
            // Find the missing variables we need as a help message.
            $returnText = 'More information is needed: ';
            $variableText = [];
            foreach ($missingVariables as $missingVariable) {
                $variableText[] = $missingVariable['name'] . ' (eg. '.implode(', ', $missingVariable['examples']).')';
            }
            $variableText = $variableText ? implode('; ', $variableText) : '';
            return $returnText . $variableText;
        }

        // Solve
        $fork = $this->findFork();
        $firstStem = $this->findFirstStem();
        $secondStem = $this->findSecondStem();

        $firstStem->setFork($fork);
        $secondStem->setFork($fork);

        $reachDifference = $secondStem->reach() - $firstStem->reach();
        $stackDifference = $secondStem->stack() - $firstStem->stack();

        $returnText = 'This stem change will result in ';
        if ($reachDifference > 0) {
            $returnText .= 'an increase in reach of '.number_format($reachDifference, 1).' mm';
        } else if ($reachDifference == 0) {
            $returnText .= 'no change in reach';
        } else {
            $returnText .= 'a decrease in reach of '.number_format(abs($reachDifference), 1).' mm';
        }
        $returnText .= ' and ';
        if ($stackDifference > 0) {
            $returnText .= 'an increase in stack of '.number_format($stackDifference, 1).' mm';
        } else if ($stackDifference == 0) {
            $returnText .= 'no change in reach';
        } else {
            $returnText .= 'a decrease in stack of '.number_format(abs($stackDifference), 1).' mm';
        }
        return $returnText;
    }

    /**
     * Finds the missing variables.
     * @return Collection
     */
    protected function missingVariables()
    {
        $missing = new Collection();
        foreach ($this->variables as $variable) {
            if (!$this->{$variable['find_method']}()) $missing->push($variable);
        }
        return $missing;
    }

    /**
     * Process an input string and populate the tokens collection.
     * @param $string
     */
    protected function tokenize($string)
    {
        $parts = preg_split('@\s+@msi', $string);
        $this->tokens = new Collection($parts);
    }

    /**
     * Create a collection of stems.
     * @return Collection[Stem]
     */
    protected function findStems()
    {
        $stems = new Collection();
        foreach ($this->tokens as $token) {
            if (preg_match('@^(?<length>[\d]+)\/(?<angle>[-+]?[\d]+)\/(?<spacers>[\d]+)(?:\/(?<height>[\d]+))?$$@msi', $token, $groups)) {
                $stem = new Stem();
                $stem->setLength($groups[1])->setAngle($groups[2])->setSpacerStack($groups[3]);
                if (isset($groups[4])) $stem->setHeight($groups[4]);
                $stems->push($stem);
            }
        }
        return $stems;
    }

    /**
     * Get the first stem.
     * @return Stem|null
     */
    protected function findFirstStem()
    {
        $stems = $this->findStems();
        if ($stems->isNotEmpty()) return $stems->first();
        return null;
    }

    /**
     * Get the second stem.
     * @return Stem|null
     */
    protected function findSecondStem()
    {
        $stems = $this->findStems();
        if ($stems->count() >= 2) return $stems->offsetGet(1);
        return null;
    }

    /**
     * Find the fork.
     * @return Fork
     */
    protected function findFork()
    {
        $fork = new Fork();

        foreach ($this->tokens as $token) {
            if (preg_match('@^(\d*\.?\d+)$@msi', $token, $group)) {
                $fork->setMountedAngle((float) $group[1]);
            }
        }

        if (!$fork->getMountedAngle()) $fork->setMountedAngle(72.5);
        return $fork;
    }
}
