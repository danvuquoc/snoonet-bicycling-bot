<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/9/17
 * Time: 9:14 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Weather;

use Danvuquoc\BicyclingBot\Bot\Plugins\Location\Models\Location;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Geocoder;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Exception;
use Cmfcmf\OpenWeatherMap;
use Cmfcmf\OpenWeatherMap\CurrentWeather;
use Cmfcmf\OpenWeatherMap\WeatherForecast;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string The plugin's command.
     */
    protected $command = 'w';

    /**
     * @var OpenWeatherMap Weather client.
     */
    protected $weatherClient;

    /**
     * Plugin constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->weatherClient = new OpenWeatherMap($config['open_weather_map_key']);
    }

    /**
     * Sub-command routing
     * @return array
     */
    public function routing()
    {
        return [
            '' => 'userWeather',
            '*' => 'queryWeather',
            'help' => 'help',
        ];
    }

    public function userWeather(CommandEventInterface $event, EventQueueInterface $queue, $nick = null)
    {
        if (!$nick) $nick = $event->getNick();

        try {
            if ($location = Location::whereSource($event->getSource())->whereNick($nick)->first()) {
                // We've found a location for the command argument as a nickname.
                $locationArray = [
                    'lat' => $location->latitude,
                    'lon' => $location->longitude,
                ];
            } else {
                // We don't know shit.
                $queue->ircPrivmsg(
                    $event->getSource(),
                    sprintf(
                        "%s does not have a location set.",
                        $nick
                    )
                );
                return;
            }
            // Make the weather query.
            $current = $this->weatherClient->getWeather($locationArray);
            $forecast = $this->weatherClient->getWeatherForecast($locationArray);
            // Pass the information to the source.
            $queue->ircPrivmsg($event->getSource(), $this->formatWeather($current, $forecast));
        } catch (Exception $e) {
            $queue->ircPrivmsg($event->getSource(), sprintf('Sorry but I could not find the weather for: %s', $nick));
        }
    }

    /**
     * Handle the weather command.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function queryWeather(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        try {
            if (Location::whereSource($event->getSource())->whereNick($commandArgument)->first()) {
                $this->userWeather($event, $queue, $commandArgument);
                return;
            } else {
                // Geocode the location.
                $geocodedLocations = Geocoder::geocode($commandArgument)->get();
                $locationArray = [
                    'lat' => $geocodedLocations->first()->getLatitude(),
                    'lon' => $geocodedLocations->first()->getLongitude(),
                ];
            }

            // Make the weather query.
            $current = $this->weatherClient->getWeather($locationArray);
            $forecast = $this->weatherClient->getWeatherForecast($locationArray);
            // Pass the information to the source.
            $queue->ircPrivmsg($event->getSource(), $this->formatWeather($current, $forecast));
        } catch (Exception $e) {
            $queue->ircPrivmsg($event->getSource(), sprintf('%s, sorry but I could not find the weather for: %s', $event->getNick(), $commandArgument));
        }
    }

    /**
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function help(CommandEventInterface $event, EventQueueInterface $queue)
    {
    }

    /**
     * Create a string representing the current and forecast weather.
     * @param CurrentWeather $current
     * @param WeatherForecast $forecast
     * @return string
     */
    protected function formatWeather(CurrentWeather $current, WeatherForecast $forecast)
    {
        return sprintf(
            "%s / %s / %s / Humidity: %s / Wind: %s at %s / Cloud Cover: %s%% / High: %s Low: %s / Outlook: %s",
            $current->city->name,
            ucwords($current->weather->description),
            $current->temperature->getFormatted(),
            $current->humidity->getFormatted(),
            $current->wind->direction->getDescription(),
            $current->wind->speed->getFormatted(),
            $current->clouds->getFormatted(),
            $current->temperature->max->getFormatted(),
            $current->temperature->min->getFormatted(),
            ucwords($forecast->current()->weather->description)
        );
    }
}
