<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/16/17
 * Time: 8:17 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto
 *
 * @property int $id
 * @property string $content
 * @property string $source
 * @property string $nick
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BikePhoto extends Model
{
    public function __toString()
    {
        return $this->content;
    }
}
