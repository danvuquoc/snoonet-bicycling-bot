<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/15/17
 * Time: 10:18 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto;

use Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Models\BikePhoto;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Exception;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string Command
     */
    protected $command = 'bikephoto';

    /**
     * @return array Routing
     */
    public function routing()
    {
        return [
            '' => 'getRequesterBikePhoto',
            '*' => 'getUserBikePhoto',
            'set' => 'setBikePhoto',
            'unset' => 'unsetBikePhoto',
        ];
    }

    /**
     * Get the bike photo of the person requesting.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function getRequesterBikePhoto(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $this->getUserBikePhoto($event, $queue, $event->getNick());
    }

    /**
     * Get the stored bike photo for the current user.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $nick
     */
    public function getUserBikePhoto(CommandEventInterface $event, EventQueueInterface $queue, $nick)
    {
        $bikePhoto = BikePhoto::whereSource($event->getSource())
            ->whereNick($nick)
            ->first();

        if ($bikePhoto) {
            $queue->ircPrivmsg(
                $event->getSource(),
                (string) $bikePhoto
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s doesn't have a bike photo set.",
                    $nick
                )
            );
        }
    }

    /**
     * Unset a bike photo.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function unsetBikePhoto(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $bikePhoto = BikePhoto::whereSource($event->getSource())
            ->whereNick($event->getNick())
            ->first();
        if ($bikePhoto) {
            $bikePhoto->delete();
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your bike photo has been removed.",
                    $event->getNick()
                )
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, you don't have a bike photo set.",
                    $event->getNick()
                )
            );
        }
    }

    /**
     * Set a bike photo.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $commandArgument
     */
    public function setBikePhoto(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        try {
            // Create and save the bike photo.
            $bikePhoto = new BikePhoto();
            $bikePhoto->source = $event->getSource();
            $bikePhoto->nick = $event->getNick();
            $bikePhoto->content = $commandArgument;
            $bikePhoto->save();
            // Output confirmation message
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, your bike photo has been added.",
                    $event->getNick()
                )
            );
        } catch (Exception $e) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry %s, an error occurred setting your bike photo: ".$e->getMessage(),
                    $event->getNick(),
                    $commandArgument
                )
            );
        }
    }
}
