<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/7/17
 * Time: 9:44 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Quote;

use Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;
use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;

class Plugin extends SubCommandRouterAbstractPlugin
{
    /**
     * @var string The plugin's command.
     */
    protected $command = 'quote';

    /**
     * @var Quote The last quote displayed.
     */
    protected $lastQuote;

    /**
     * Subcommand routing
     * @return array
     */
    public function routing()
    {
        return [
            '' => 'random',
            'help' => 'help',
            'add' => 'add',
            'info' => 'info',
            'search' => 'search',
        ];
    }

    /**
     * Pick a random quote to output.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function random(CommandEventInterface $event, EventQueueInterface $queue)
    {
        if ($quote = Quote::whereSource($event->getSource())->inRandomOrder()->take(1)->first()) {
            $this->lastQuote = $quote;
            $queue->ircPrivmsg($event->getSource(), $quote->quote);
        } else {
            $queue->ircPrivmsg($event->getSource(), "There are no quotes in the database yet.");
        }
    }

    public function info(CommandEventInterface $event, EventQueueInterface $queue)
    {
        if ($this->lastQuote) {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Quote added by %s on %s",
                    $this->lastQuote->nick,
                    $this->lastQuote->created_at
                )
            );
        } else {
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s, no quotes have recently been retrieved.",
                    $event->getNick()
                )
            );
        }
    }

    /**
     * Add a quote.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $commandArgument
     */
    public function add(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        if ($quote = Quote::whereSource($event->getSource())->whereQuote($commandArgument)->first()) {
            // Quote already exists.
            $queue->ircPrivmsg($event->getSource(), sprintf('%s, sorry that quote already exists.', $event->getNick()));
        } else {
            // Create a new quote.
            $quote = new Quote();
            $quote->source = $event->getSource();
            $quote->nick = $event->getNick();
            $quote->user = $event->getUsername();
            $quote->host = $event->getHost();
            $quote->quote = $commandArgument;
            $quote->save();
            $queue->ircPrivmsg($event->getSource(), sprintf('%s, quote added.', $event->getNick()));
        }
    }

    /**
     * Help statement
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $commandArgument
     */
    public function help(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        // TODO Help
    }

    /**
     * Search for a quote.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param string $commandArgument
     */
    public function search(CommandEventInterface $event, EventQueueInterface $queue, $commandArgument)
    {
        if ($quote = Quote::whereSource($event->getSource())->where('quote', 'like', '%'.$commandArgument.'%')->inRandomOrder()->take(1)->first()) {
            $this->lastQuote = $quote;
            $queue->ircPrivmsg($event->getSource(), $quote->quote);
        } else {
            $queue->ircPrivmsg($event->getSource(), 'No quote found containing: '.$commandArgument);
        }
    }
}
