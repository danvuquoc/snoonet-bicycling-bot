<?php

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote
 *
 * @property int $id
 * @property string $source
 * @property string $nick
 * @property string $user
 * @property string $host
 * @property string $quote
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereHost($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereNick($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereQuote($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Models\Quote whereUser($value)
 * @mixin \Eloquent
 */
class Quote extends Model
{
    //
}
