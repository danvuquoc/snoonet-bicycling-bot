<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/25/17
 * Time: 12:48 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Zwift;

use Phergie\Irc\Plugin\React\Command\CommandEventInterface;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Danvuquoc\BicyclingBot\Bot\Plugins\SubCommandRouterAbstractPlugin;

class Plugin extends SubCommandRouterAbstractPlugin
{

    protected $command = 'zwift';

    /**
     * Routing for parameters.
     * @return array
     */
    public function routing()
    {
        return [
            '' => 'currentMap',
            '*' => 'specificMap',
            'help' => 'help',
        ];
    }

    /**
     * Help
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function help(CommandEventInterface $event, EventQueueInterface $queue)
    {
        $messages = [
            'This command displays information regarding maps playing on Zwift.',
            "Usage: !{$this->command}, !{$this->command} [map name]",
        ];

        foreach ($messages as $message) {
            $queue->ircPrivmsg($event->getSource(), $message);
        }
    }

    /**
     * Get zwift info.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     * @param $map
     */
    public function specificMap(CommandEventInterface $event, EventQueueInterface $queue, $map)
    {
        $schedule = new Schedule();
        $current = $schedule->getCurrent();

        // A map was specified.
        if ($current['map'] == strtoupper($map)) {
            // We're currently on the map.
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s is already the current map for another %s",
                    $schedule->getCurrentMap(),
                    $schedule->getTimeTillNext()
                )
            );
        } else if ($schedule->getNext($map)) {
            // We've found the next map.
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "%s will start in %s",
                    $schedule->getNextMap($map),
                    $schedule->getTimeTillNext($map)
                )
            );
        } else {
            // No schedule for the next map specified.
            $queue->ircPrivmsg(
                $event->getSource(),
                sprintf(
                    "Sorry, %s has not been scheduled yet.",
                    $map
                )
            );
        }
    }

    /**
     * Handle the zwift command without a map specified.
     * @param CommandEventInterface $event
     * @param EventQueueInterface $queue
     */
    public function currentMap(CommandEventInterface $event, EventQueueInterface $queue)
    {
        // Current and next map.
        $schedule = new Schedule();
        $currentMap = $schedule->getCurrentMap();
        $nextMap = $schedule->getNextMap();
        $timeTillNext = $schedule->getTimeTillNext();

        $queue->ircPrivmsg(
            $event->getSource(),
            sprintf(
                "The current Zwift map is %s, %s will start in %s.",
                $currentMap,
                $nextMap,
                $timeTillNext
            )
        );
    }
}