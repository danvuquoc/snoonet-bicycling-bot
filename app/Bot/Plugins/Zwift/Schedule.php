<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 2/25/17
 * Time: 12:56 PM
 */

namespace Danvuquoc\BicyclingBot\Bot\Plugins\Zwift;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use SimpleXMLElement;

class Schedule
{
    const SCHEDULE_XML_URL = 'http://whatsonzwift.com/MapSchedule.xml';

    protected $schedule;

    public function __construct() {
        $this->schedule = new Collection();
        $this->initializeSchedule();
    }

    /**
     * Initialize the schedule.
     */
    protected function initializeSchedule()
    {
        $scheduleXml = new SimpleXMLElement(file_get_contents(self::SCHEDULE_XML_URL));
        foreach ($scheduleXml->xpath('//appointment') as $appointment) {
            $this->schedule->push([
                'map' => strtoupper($appointment['map']),
                'start' => Carbon::parse($appointment['start'])
            ]);
        }
        
        $this->schedule = $this->schedule->sortBy('start');
    }

    /**
     * Get the next appointment.
     * @param string $map An optional map to specify.
     * @return array | null The appointment.
     */
    public function getNext($map = null)
    {
        $now = Carbon::create();
        foreach($this->schedule as $appointment) {
            if ($map && strtoupper($map) != $appointment['map']) continue;
            if ($appointment['start'] >= $now) {
                return $appointment;
            }
        }
        return null;
    }

    /**
     * Get the current appointment.
     * @return array | null The appointment.
     */
    public function getCurrent()
    {
        $now = Carbon::now();
        foreach($this->schedule->reverse() as $appointment) {
            if ($appointment['start'] <= $now) {
                return $appointment;
            }
        }
        return null;
    }

    /**
     * Get the next map name.
     * @param null $map
     * @return string
     */
    public function getNextMap($map = null)
    {
        if ($appointment = $this->getNext($map)) {
            return ucwords(strtolower($appointment['map']));
        }
    }

    /**
     * Get the current map name.
     * @return string
     */
    public function getCurrentMap()
    {
        if ($appointment = $this->getCurrent()) {
            return ucwords(strtolower($appointment['map']));
        }
    }

    /**
     * Get the time until next map.
     * @param null $map
     * @return mixed
     */
    public function getTimeTillNext($map = null)
    {
        $nextAppointment = $this->getNext($map);
        return $nextAppointment['start']->diff(Carbon::now())->format('%d days %H hours %i minutes %s seconds');
    }

    /**
     * Get the schedule collection.
     * @return Collection
     */
    public function getSchedule()
    {
        return $this->schedule;
    }
}