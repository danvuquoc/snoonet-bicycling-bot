<?php

namespace Danvuquoc\BicyclingBot\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     */
    public function register()
    {
        //
    }
}
