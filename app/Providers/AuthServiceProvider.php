<?php

namespace Danvuquoc\BicyclingBot\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Danvuquoc\BicyclingBot\Model' => 'Danvuquoc\BicyclingBot\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
