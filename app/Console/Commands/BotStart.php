<?php

namespace Danvuquoc\BicyclingBot\Console\Commands;

use Illuminate\Console\Command;
use Phergie\Irc\Connection;
use Phergie\Irc\Bot\React\Bot;
use ReflectionClass;
use Log;

class BotStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start the bot.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bot = new Bot();
        $bot->setConfig($this->config());
        $bot->run();
    }

    /**
     * Create a configuration array.
     * @return array
     */
    public function config()
    {
        // Setup the configuration array.
        $config = [
            'plugins' => [],
            'connections' => [],
            'logger' => Log::getMonolog()
        ];

        // Attach the server connection.
        $config['connections'][] = new Connection($this->connectionConfiguration());

        // Required plugins: pong, command, auto-join, auto rejoin, nickserv
        // These are loaded based on configuration only as needed.
        $config['plugins'][] = new \Phergie\Irc\Plugin\React\Pong\Plugin();
        $config['plugins'][] = new \Phergie\Irc\Plugin\React\Command\Plugin(['prefix' => config('bot.command_prefix')]);
        if (config('bot.channels')) {
            $config['plugins'][] = new \Phergie\Irc\Plugin\React\AutoJoin\Plugin(['channels' => config('bot.channels')]);
            if ('bot.rejoin') {
                $config['plugins'][] = new \EnebeNb\Phergie\Plugin\AutoRejoin\Plugin(['channels' => config('bot.channels')]);
            }
        }
        if (config('bot.nickserv_password')) {
            $config['plugins'][] = new \Phergie\Irc\Plugin\React\NickServ\Plugin([
                'password' => config('bot.nickserv_password'),
                'botnick' => config('bot.nickserv_nick')]
            );
        }

        // Additional plugins to be loaded.
        if (config('bot.plugins')) {
            foreach (config('bot.plugins') as $pluginArray) {
                $pluginReflectionClass = new ReflectionClass($pluginArray['plugin']);
                $config['plugins'][] =  $pluginReflectionClass->newInstanceArgs($pluginArray['constructor_args']);
            }
        }

        return $config;
    }

    /**
     * Generate a connection configuration based on config/bot.php
     * @return array
     */
    protected function connectionConfiguration()
    {
        $connectionOptions = [
            'serverHostname' => config('bot.hostname'),
            'username' => config('bot.username'),
            'realname' => config('bot.realname'),
            'nickname' => config('bot.nickname'),
            'serverport' => config('bot.port')
        ];

        // Do we want ssl?
        if (config('bot.ssl')) {
            $connectionOptions['options']['transport'] = 'ssl';
        }

        return $connectionOptions;
    }
}
