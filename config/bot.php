<?php

return [
    'hostname' => env('IRCBOT_HOSTNAME'),
    'port' => env('IRCBOT_PORT', 6667),
    'ssl' => env('IRCBOT_SSL', false),
    'username' => env('IRCBOT_USERNAME'),
    'realname' => env('IRCBOT_REALNAME'),
    'nickname' => env('IRCBOT_NICKNAME'),
    'nickserv_password' => env('IRCBOT_NICKSERV_PASSWORD'),
    'nickserv_nick' => env('IRCBOT_NICKSERV_NICK', 'NickServ'),
    'channels' => env('IRCBOT_CHANNELS'),
    'rejoin' => env('IRCBOT_REJOIN', false),
    'admins' => env('IRCBOT_ADMINS'),
    'command_prefix' => env('IRCBOT_COMMAND_PREFIX', '!'),
    'plugins' => [
        [
            'plugin' => \Phergie\Irc\Plugin\React\JoinPart\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Phergie\Irc\Plugin\React\Quit\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\BikePhoto\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\GearCalc\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Location\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Photo\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Quote\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Title\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Units\Plugin::class,
            'constructor_args' => [],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Weather\Plugin::class,
            'constructor_args' => [
                'open_weather_map_key' => env('IRCBOT_API_OWM_KEY'),
            ],
        ],
        [
            'plugin' => \Danvuquoc\BicyclingBot\Bot\Plugins\Zwift\Plugin::class,
            'constructor_args' => [],
        ],
    ],
];
