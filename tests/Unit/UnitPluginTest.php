<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 3/1/17
 * Time: 7:37 PM
 */

namespace Tests\Unit;


use Danvuquoc\BicyclingBot\Bot\Plugins\Units\Plugin;
use Phergie\Irc\Event\UserEventInterface;
use Tests\TestCase;
use Mockery;

class UnitPluginTest extends TestCase
{
    public function createEvent($source, $nick, $line)
    {
        $eventMock = Mockery::mock(UserEventInterface::class);
        $eventMock->shouldReceive('getSource')->andReturn($source);
        $eventMock->shouldReceive('getParams')->andReturn(['text' => $line]);
        $eventMock->shouldReceive('getNick')->andReturn($nick);
        return $eventMock;
    }

    public function testHistory()
    {
        // Setup
        $unitPlugin = new Plugin();

        // Make sure nothing is in there.
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling');
        $this->assertNull($lastLine);

        // Push a line in and make sure it exists.
        $unitPlugin->listen($this->createEvent('#bicycling', 'nick1', '100 l'));
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling');
        $this->assertEquals('<nick1> 100 l', $lastLine);

        // Make sure there aren't any lines for another channel.
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling1');
        $this->assertNull($lastLine);

        // Push another line from another nickname.
        $unitPlugin->listen($this->createEvent('#bicycling', 'nick2', '100 mph'));
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling');
        $this->assertEquals('<nick2> 100 mph', $lastLine);

        // Ask for the first line by nick1
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling', 'nick1');
        $this->assertEquals('<nick1> 100 l', $lastLine);

        // Ask for the first line by nick2
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling', 'nick2');
        $this->assertEquals('<nick2> 100 mph', $lastLine);

        // Ask for a nick that hasn't been seen yet.
        $lastLine = $unitPlugin->getLastChannelLine('#bicycling', 'nick3');
        $this->assertNull($lastLine);
    }
}