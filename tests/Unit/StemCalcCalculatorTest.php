<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 3/5/17
 * Time: 8:18 AM
 */

namespace Tests\Unit;

use Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc\Calculator;
use Tests\TestCase;
use ReflectionClass;

class StemCalcCalculatorTest extends TestCase
{
    public static function callMethod($object, $method, array $args = [])
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }

    public function testFindFork()
    {
        $calc = new Calculator('100/-6/25/40 110/-6/25/40');
        $fork = self::callMethod($calc, 'findFork');
        $this->assertEquals(72.5, $fork->getMountedAngle());

        $calc = new Calculator('100/-6/25/40 110/-6/25/40 71');
        $fork = self::callMethod($calc, 'findFork');
        $this->assertEquals(71, $fork->getMountedAngle());
    }

    public function testSolve()
    {
        $calc = new Calculator('100/-6/25 110/-6/25');
        $this->assertEquals('This stem change will result in an increase in reach of 9.8 mm and an increase in stack of 2.0 mm', $calc->solve());

        $calc = new Calculator('100/-6/25/40 110/-6/25/40');
        $this->assertEquals('This stem change will result in an increase in reach of 9.8 mm and an increase in stack of 2.0 mm', $calc->solve());
    }
}