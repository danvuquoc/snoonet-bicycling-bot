<?php

namespace Tests\Unit;

use Danvuquoc\BicyclingBot\Bot\Plugins\Units\Models\UnitOfMeasure;
use PhpUnitsOfMeasure\PhysicalQuantity\Area;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Pressure;
use PhpUnitsOfMeasure\PhysicalQuantity\Temperature;
use PhpUnitsOfMeasure\PhysicalQuantity\Velocity;
use PhpUnitsOfMeasure\PhysicalQuantity\Volume;
use Tests\TestCase;

class UnitOfMeasureTest extends TestCase
{
    public function testFindEmpty()
    {
        $line = '';
        $this->assertEquals(true, UnitOfMeasure::find($line)->isEmpty());
    }

    public function testFindCommas()
    {
        $line = '100m,100 m, 100m, 100 m,';
        $this->assertEquals(4, UnitOfMeasure::find($line)->count());
    }

    public function testFindPeriods()
    {
        $line = '100m. 100 m.';
        $this->assertEquals(2, UnitOfMeasure::find($line)->count());
    }

    public function testFindNonUnit()
    {
        $line = '100 chickens';
        $this->assertEquals(true, UnitOfMeasure::find($line)->isEmpty());
    }

    public function testFindLength()
    {
        $line = '100 meters';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Length::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindVelocity()
    {
        $line = '100 mph';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Velocity::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindMass()
    {
        $line = '100 lbs';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Mass::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindArea()
    {
        $line = '100m^2';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Area::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindTemperature()
    {
        $line = '100 F';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Temperature::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindPressure()
    {
        $line = '100 psi';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Pressure::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testFindVolume()
    {
        $line = '100 l';
        $this->assertEquals(1, UnitOfMeasure::find($line)->count());
        $this->assertEquals(Volume::class, get_class(UnitOfMeasure::find($line)->first()->getPhysicalProperty()));
    }

    public function testLengthMetric()
    {
        $line = '100m 100 km';
        $convertedLine = '328.08 ft 62.14 mi';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testLengthImperial()
    {
        $line = '100ft 100 mi';
        $convertedLine = '30.48 m 160.93 km';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testVelocityMetric()
    {
        $line = '100 km/h';
        $convertedLine = '62.14 mph';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testVelocityImperial()
    {
        $line = '100 mph';
        $convertedLine = '160.93 km/h';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testTemperatureMetric()
    {
        $line = '100 C';
        $convertedLine = '212 F';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testTemperatureImperial()
    {
        $line = '100 C';
        $convertedLine = '212 F';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testAreaMetric()
    {
        $line = '100 m^2';
        $convertedLine = '1076.39 ft^2';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testAreaImperial()
    {
        $line = '100 ft^2';
        $convertedLine = '9.29 m^2';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testPressureMetric()
    {
        $line = '100 bar';
        $convertedLine = '1450.38 psi';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testPressureImperial()
    {
        $line = '100 psi';
        $convertedLine = '6.89 bar';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testVolumeMetric()
    {
        $line = '100 l';
        $convertedLine = '105.67 qt';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }

    public function testVolumeImperial()
    {
        $line = '100 quarts';
        $convertedLine = '94.64 l';
        $this->assertEquals($convertedLine, UnitOfMeasure::switchUnits($line));
    }
}
