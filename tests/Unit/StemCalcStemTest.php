<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 3/5/17
 * Time: 8:18 AM
 */

namespace Tests\Unit;

use Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc\Stem;
use Danvuquoc\BicyclingBot\Bot\Plugins\StemCalc\Fork;
use Tests\TestCase;
use ReflectionClass;

class StemCalcStemTest extends TestCase
{
    public static function callMethod($object, $method, array $args = [])
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }

    public function testCalculateReach()
    {
        $fork = Fork::create()->setMountedAngle(72.5);
        $stem = Stem::create()->setLength(100)->setAngle(-6)->setHeight(40)->setSpacerStack(25)->setFork($fork);
        $this->assertEquals(84, $stem->reach(), '', 1);
    }

    public function testCalculateReachWhenForkNotSet()
    {
        $this->expectException(\Exception::class);
        $stem = Stem::create()->setLength(100)->setAngle(-6)->setHeight(40)->setSpacerStack(25);
        $stem->reach();
    }

    public function testCalculateStack()
    {
        $fork = Fork::create()->setMountedAngle(72.5);
        $stem = Stem::create()->setLength(100)->setAngle(-6)->setHeight(40)->setSpacerStack(25)->setFork($fork);
        $this->assertEquals(62, $stem->stack(), '', 1);
    }

    public function testCalculateStackWhenForkNotSet()
    {
        $this->expectException(\Exception::class);
        $stem = Stem::create()->setLength(100)->setAngle(-6)->setHeight(40)->setSpacerStack(25);
        $stem->stack();
    }
}