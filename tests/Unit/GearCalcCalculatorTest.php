<?php
/**
 * Created by PhpStorm.
 * User: danvuquoc
 * Date: 3/5/17
 * Time: 8:18 AM
 */

namespace Tests\Unit;

use Danvuquoc\BicyclingBot\Bot\Plugins\GearCalc\Calculator;
use PhpUnitsOfMeasure\PhysicalQuantity\Velocity;
use Tests\TestCase;
use ReflectionClass;

class GearCalcCalculatorTest extends TestCase
{
    public static function callMethod($object, $method, array $args = [])
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }

    public function testFindCadence()
    {
        $calc = new Calculator('');
        $this->assertNull(self::callMethod($calc, 'findCadence'));

        $calc = new Calculator('100irrelenvant text');
        $this->assertNull(self::callMethod($calc, 'findCadence'));

        $calc = new Calculator('100rpm');
        $this->assertEquals(100, self::callMethod($calc, 'findCadence'));
    }

    public function testFindSpeed()
    {
        $calc = new Calculator('');
        $this->assertNull(self::callMethod($calc, 'findSpeed'));

        $calc = new Calculator('100rpm');
        $this->assertNull(self::callMethod($calc, 'findSpeed'));

        $calc = new Calculator('100mph');
        $this->assertEquals(Velocity::class, get_class(self::callMethod($calc, 'findSpeed')));

        $calc = new Calculator('100km/h');
        $this->assertEquals(Velocity::class, get_class(self::callMethod($calc, 'findSpeed')));

        $calc = new Calculator('100m/s');
        $this->assertEquals(Velocity::class, get_class(self::callMethod($calc, 'findSpeed')));
    }

    public function testFindFrontTeeth()
    {
        $calc = new Calculator('');
        $this->assertNull(self::callMethod($calc, 'findFrontTeeth'));

        $calc = new Calculator('100irrelenvant text');
        $this->assertNull(self::callMethod($calc, 'findFrontTeeth'));

        $calc = new Calculator('?x11');
        $this->assertNull(self::callMethod($calc, 'findFrontTeeth'));

        $calc = new Calculator('53x11');
        $this->assertEquals(53, self::callMethod($calc, 'findFrontTeeth'));

        $calc = new Calculator('53x?');
        $this->assertEquals(53, self::callMethod($calc, 'findFrontTeeth'));
    }

    public function testFindRearTeeth()
    {
        $calc = new Calculator('');
        $this->assertNull(self::callMethod($calc, 'findRearTeeth'));

        $calc = new Calculator('100irrelenvant text');
        $this->assertNull(self::callMethod($calc, 'findRearTeeth'));

        $calc = new Calculator('53x?');
        $this->assertNull(self::callMethod($calc, 'findRearTeeth'));

        $calc = new Calculator('53x11');
        $this->assertEquals(11, self::callMethod($calc, 'findRearTeeth'));

        $calc = new Calculator('?x11');
        $this->assertEquals(11, self::callMethod($calc, 'findRearTeeth'));
    }

    public function testFindWheelCircumference()
    {
        $calc = new Calculator('');
        $this->assertEquals(2099, self::callMethod($calc, 'findWheelCircumference'));

        $calc = new Calculator('23-622');
        $this->assertEquals(2099, self::callMethod($calc, 'findWheelCircumference'));

        $calc = new Calculator('28-622');
        $this->assertEquals(2130, self::callMethod($calc, 'findWheelCircumference'));
    }

    public function testSolve()
    {
        $calc = new Calculator('');
        $this->assertEquals(false, $calc->isSolvable());

        $calc = new Calculator('50x11 100rpm');
        $this->assertEquals('57.2 km/h', $calc->solve());

        $calc = new Calculator('50x11 100rpm imperial');
        $this->assertEquals('35.6 mph', $calc->solve());

        $calc = new Calculator('50x11 100rpm', false);
        $this->assertEquals('35.6 mph', $calc->solve());

        $calc = new Calculator('50x11 100rpm metric', false);
        $this->assertEquals('57.2 km/h', $calc->solve());

        $calc = new Calculator('50x11 57.2km/h');
        $this->assertEquals('100 rpm', $calc->solve());

        $calc = new Calculator('?x11 100rpm 57.2km/h');
        $this->assertEquals('50T chainring', $calc->solve());

        $calc = new Calculator('50x? 100rpm 57.2km/h');
        $this->assertEquals('11T cog', $calc->solve());
    }
}